**GOAL**

In this repository, you may find the source code of our Operating system team project in EURECOM to build a robot for a "Relay Race".

Our robot is, **Bobert**.

The directory **FirstSession** contains the files used for the first assessment.

The directory **FinalRobot** contains the files used for the final assessment.


**TEAM CONTRIBUTION**

The team members are Rihab Harrack, Prashant Kumar Dey and Honey Susan Kurian. Just a small description about the work done by each of us. 
Rihab and Honey worked on the construction of the robot. 

Before the first assessment on 13th December 2016, Prashant and Honey worked on the code. We did a lot of manipulation and tests with each of the wheels of the robot as they were not synchronised. 

After the first assessment, we were given new motors for the wheels. Hence, once we were back from Christmas holidays, we re-designed the robot and changed the code completely as the new wheels were synchronous.
Honey and Rihab worked on grabbing and dropping the ball, the robot movement and the project report. Prashant worked on communication with the server using bluetooth and designing our fabulous website.

We didn't use any repository during coding as we used to sit together and code/discuss and the updated code was always on the robot. Hence, no timeline of commits can be observed.


For more information about the project report along with videos and pictures, you can access our website, http://robot.prashantdey.in/bobert/index.html