//Port C and Port D for Motors to move
//Right Motor - Port C - 67
//Left Motor - Port D - 68
//For Right turn, side value should be 0



#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"

#ifdef __WIN32__

#include <windows.h>

#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

#endif

const char const *color[] = { "?", "BLACK", "BLUE", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))


int forward_bobert(int k, int psp){
	int flag1 = 1;
	uint8_t sn;
	
	while (flag1<=k){
		Sleep(15);
		if ( ev3_search_tacho_plugged_in(67, 0, &sn, 0 )) {
		int max_speed;
		
		get_tacho_max_speed( sn, &max_speed );
		set_tacho_stop_action_inx( sn, TACHO_COAST );
		set_tacho_speed_sp( sn, max_speed * 1/5 );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
		
	} else {
		printf( "LEGO_EV3_L_MOTOR 2 is NOT found\n" );
	}
				
	Sleep(10);
	if ( ev3_search_tacho_plugged_in(68, 0, &sn, 0 )) {
		int max_speed;
		get_tacho_max_speed( sn, &max_speed );
		
		set_tacho_stop_action_inx( sn, TACHO_COAST );
		set_tacho_speed_sp( sn, max_speed * 1/5 );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
	} else {
		printf( "LEGO_EV3_L_MOTOR 3 is NOT found\n" );
	}	
	flag1++;
		
	}

	return 0;
}

int backward_bobert(int k, int psp){
	
	int flag1 = 1;
	uint8_t sn;
	
	while (flag1<=k){
		Sleep(15);
		if ( ev3_search_tacho_plugged_in(67, 0, &sn, 0 )) {
		int max_speed;
		
		get_tacho_max_speed( sn, &max_speed );
		set_tacho_stop_action_inx( sn, TACHO_COAST );
		set_tacho_speed_sp( sn, -max_speed * 1/5 );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
		
	} else {
		printf( "LEGO_EV3_L_MOTOR 2 is NOT found\n" );
	}
				
	Sleep(10);
	if ( ev3_search_tacho_plugged_in(68, 0, &sn, 0 )) {
		int max_speed;
		get_tacho_max_speed( sn, &max_speed );
		
		set_tacho_stop_action_inx( sn, TACHO_COAST );
		set_tacho_speed_sp( sn, -max_speed * 1/5 );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
	} else {
		printf( "LEGO_EV3_L_MOTOR 3 is NOT found\n" );
	}	
	flag1++;
		
	}

	return 0;
	
}


int wheel(int port, int psp){
	
	uint8_t sn;	
	Sleep(15);
	
		if ( ev3_search_tacho_plugged_in(abs(port), 0, &sn, 0 )) {
		int max_speed;
		
		get_tacho_max_speed( sn, &max_speed );
		set_tacho_stop_action_inx( sn, TACHO_HOLD );
		set_tacho_speed_sp( sn, max_speed * 1 / 10 );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
		
	} else {
		printf( "LEGO_EV3_L_MOTOR 2 is NOT found\n" );
	}
	/*
		if ( ev3_search_tacho_plugged_in(abs(port+1), 0, &sn, 0 )) {
		int max_speed;
		
		get_tacho_max_speed( sn, &max_speed );
		set_tacho_stop_action_inx( sn, TACHO_HOLD );
		set_tacho_speed_sp( sn, max_speed * 1 / 10 );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, -psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
		
	} else {
		printf( "LEGO_EV3_L_MOTOR 2 is NOT found\n" );
	}*/
	
	
	return 0;

}

/* ---------------------------------------------------------------------------------------------------  */
// working with hands

int hand_open(){
	uint8_t sn;
	
	if ( ev3_search_tacho_plugged_in(65, 0, &sn, 0 )) {
		int max_speed;
		printf( "Opening Hand...\n" );
		get_tacho_max_speed( sn, &max_speed );
		set_tacho_stop_action_inx( sn, TACHO_COAST);
		set_tacho_speed_sp( sn, max_speed * 1 / 6 );
		set_tacho_time_sp( sn, 5000 );
		set_tacho_ramp_up_sp( sn, 2000 );
		set_tacho_ramp_down_sp( sn, 2000 );
		set_tacho_command_inx( sn, TACHO_RUN_TIMED );
					
		} 
		return 0;
	}


int hand_close(){
	uint8_t sn;
	if ( ev3_search_tacho_plugged_in(65, 0, &sn, 0 )) {
		int max_speed;
		printf( "Closing Hand...\n" );
		get_tacho_max_speed( sn, &max_speed );
		set_tacho_stop_action_inx( sn, TACHO_COAST);
		set_tacho_speed_sp( sn, -max_speed * 1 / 6 );
		set_tacho_time_sp( sn, 5000 );
		set_tacho_ramp_up_sp( sn, 2000 );
		set_tacho_ramp_down_sp( sn, 2000 );
		set_tacho_command_inx( sn, TACHO_RUN_TIMED );
				
	} 
	return 0;
}

//Moving Hand up and down

int hand_up(){
	uint8_t sn;
	if ( ev3_search_tacho_plugged_in(66, 0, &sn, 0 )) {
		int max_speed;
		get_tacho_max_speed( sn, &max_speed );
		set_tacho_stop_action_inx( sn, TACHO_COAST);
		set_tacho_speed_sp( sn, -max_speed * 1 / 9 );
		set_tacho_time_sp( sn, 5000 );
		set_tacho_ramp_up_sp( sn, 2000 );
		set_tacho_ramp_down_sp( sn, 2000 );
		set_tacho_command_inx( sn, TACHO_RUN_TIMED );
		}
	return 0;
}


int hand_down(){
	uint8_t sn;
	if ( ev3_search_tacho_plugged_in(66, 0, &sn, 0 )) {
		int max_speed;
		get_tacho_max_speed( sn, &max_speed );
		set_tacho_stop_action_inx( sn, TACHO_COAST);
		set_tacho_speed_sp( sn, max_speed * 1 / 9 );
		set_tacho_time_sp( sn, 5000 );
		set_tacho_ramp_up_sp( sn, 2000 );
		set_tacho_ramp_down_sp( sn, 2000 );
		set_tacho_command_inx( sn, TACHO_RUN_TIMED );
					
	}
	return 0;
}

int main( void )
{
	int i,gyro_counter=1;
	int gyro_odd, gyro_exact = 0, sonar_counter=1, sonar_past, sonar_present, preserve_sonar_value;
	uint8_t sn_color;
	uint8_t sn_compass;
	uint8_t sn_sonar;
	uint8_t sn_mag;
	char s[ 256 ];
	int val, counter=0, color_counter = 1,take;
	float value;
	uint32_t n, ii;
#ifndef __ARM_ARCH_4T__
	/* Disable auto-detection of the brick (you have to set the correct address below) */
	ev3_brick_addr = "169.254.223.200";

#endif
	if ( ev3_init() == -1 ) return ( 1 );

#ifndef __ARM_ARCH_4T__
	printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

#else
	printf( "Waiting tacho is plugged...\n" );

#endif
	while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

	printf( "*** ( EV3 ) Hello! ***\n" );

	printf( "Found tacho motors:\n" );
	for ( i = 0; i < DESC_LIMIT; i++ ) {
		if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
			printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
			printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
			printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
		}
	}
	Sleep(4000);
	//Run all sensors
	ev3_sensor_init();
	
	printf( "Found sensors:\n" );
	for ( i = 0; i < DESC_LIMIT; i++ ) {
		if ( ev3_sensor[ i ].type_inx != SENSOR_TYPE__NONE_ ) {
			printf( "  type = %s\n", ev3_sensor_type( ev3_sensor[ i ].type_inx ));
			printf( "  port = %s\n", ev3_sensor_port_name( i, s ));
			if ( get_sensor_mode( i, s, sizeof( s ))) {
				printf( "  mode = %s\n", s );
			}
			if ( get_sensor_num_values( i, &n )) {
				for ( ii = 0; ii < n; ii++ ) {
					if ( get_sensor_value( ii, i, &val )) {
						printf( "  value%d = %d\n", ii, val );
					}
				}
			}
		}
	}
	int stop=1, hello;
	
	while (stop){
			
			
		if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
			printf("SONAR found, reading sonar...\n");
			if ( !get_sensor_value0(sn_sonar, &value )) {
				value = 0;
			}
			
			printf( "\r(%f) \n", value);
			fflush( stdout );
	    	}
		preserve_sonar_value = value;
		
		value = preserve_sonar_value;
		sonar_present = value;
		if (sonar_counter>=2){
			if (abs(sonar_past - sonar_present) > 300){
				printf("Sonar_past - Sonar_present = %d\n", (sonar_past - sonar_present));
				
				wheel(67, -10);				
				Sleep(300);
			}
			
		}
		
		if (stop == 1){
			
			if(value>=400){
			
			wheel(67, 25);
			Sleep(100);
		}
		
		if(value<400 && value>65){
			take = (value*45)/200;
			forward_bobert(1,take);
			hello = value;
			Sleep(300);
		}
		
		if(value<=65){
			hand_down();
			Sleep(6000);
			hand_close();
			Sleep(6000);
			hand_up();
			Sleep(6000);
			backward_bobert(2, -300);
			printf("[+] Bobert Stopping \n");
			stop = 0;
			Sleep(4000);
			Sleep(300);
		}
		sonar_past=value;	
			
		}
		
			
		
		fflush( stdout );
		Sleep( 200 );
		sonar_counter++;

	}
	ev3_uninit();
	printf( "*** ( EV3 ) Bye! ***\n" );
	return ( 0 );
}
